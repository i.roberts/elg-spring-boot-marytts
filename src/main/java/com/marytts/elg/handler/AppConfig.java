package com.marytts.elg.handler;

import eu.elg.handler.ElgHandler;
import marytts.exceptions.MaryConfigurationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @ElgHandler
    @Bean
    public MaryTTSHandler en_US_f_slt() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "US","cmu-slt-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enUS_m_bdl() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "US","cmu-bdl-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enUS_m_rms() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "US","cmu-rms-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enGB_m_obadiah() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "GB","dfki-obadiah-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enGB_m_spike() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "GB","dfki-spike-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enGB_f_prudence() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "GB","dfki-prudence-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler enGB_f_poppy() throws MaryConfigurationException {
        return new MaryTTSHandler("en", "GB","dfki-poppy-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler de_f_bits1() throws MaryConfigurationException {
        return new MaryTTSHandler("de", "","bits1-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler de_m_bits3() throws MaryConfigurationException {
        return new MaryTTSHandler("de", "","bits3-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler de_m_pavoque() throws MaryConfigurationException {
        return new MaryTTSHandler("de", "","dfki-pavoque-neutral-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler fr_f_camille() throws MaryConfigurationException {
        return new MaryTTSHandler("fr", "","enst-camille-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler fr_m_dennys() throws MaryConfigurationException {
        return new MaryTTSHandler("fr", "","enst-dennys-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler fr_f_jessica() throws MaryConfigurationException {
        return new MaryTTSHandler("fr", "","upmc-jessica-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler fr_m_pierre() throws MaryConfigurationException {
        return new MaryTTSHandler("fr", "","upmc-pierre-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler it_f_lucia() throws MaryConfigurationException {
        return new MaryTTSHandler("it", "","istc-lucia-hsmm");
    }

// These two voices are not in jcenter de.dfki.mary repo.

//    @ElgHandler
//    @Bean
//    public MaryTTSHandler ru_f_irina() throws MaryConfigurationException {
//        return new MaryTTSHandler("ru", "","ac-irina-hsmm");
//    }
//
//    @ElgHandler
//    @Bean
//    public MaryTTSHandler sv_m_hb() throws MaryConfigurationException {
//        return new MaryTTSHandler("sv", "","stts-sv-hb-hsmm");
//    }

    @ElgHandler
    @Bean
    public MaryTTSHandler te_f_nk() throws MaryConfigurationException {
        return new MaryTTSHandler("te", "","cmu-nk-hsmm");
    }

    @ElgHandler
    @Bean
    public MaryTTSHandler tr_m_ot() throws MaryConfigurationException {
        return new MaryTTSHandler("tr", "","dfki-ot-hsmm");
    }
}
