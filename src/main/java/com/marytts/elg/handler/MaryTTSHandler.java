package com.marytts.elg.handler;

import eu.elg.handler.ElgHandlerRegistrar;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AudioResponse;
import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.util.MaryRuntimeUtils;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Locale;

public class MaryTTSHandler implements eu.elg.handler.ElgHandlerRegistration {

    public void MaryTTSHandler() throws Exception {
        MaryRuntimeUtils.ensureMaryStarted();
    }

    private String voice = "";
    private String lang = "";
    private String country = "";

    private final LocalMaryInterface localMaryInterface = new LocalMaryInterface();

    public MaryTTSHandler(String lang, String country, String voice) throws MaryConfigurationException {
        this.lang = lang;
        this.country = country;
        this.voice = voice;
        this.localMaryInterface.setLocale(new Locale(this.lang, this.country));
        this.localMaryInterface.setVoice(this.voice);
    }

    public AudioResponse textToSpeech(TextRequest req) throws SynthesisException, IOException {
        AudioInputStream audio = this.localMaryInterface.generateAudio(req.getContent());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        AudioSystem.write(audio, AudioFileFormat.Type.WAVE, baos);
        byte[] buffer = baos.toByteArray();
        return new AudioResponse().withContent(buffer).withFormat(AudioResponse.Format.LINEAR16);
    }

    public void registerHandlers(ElgHandlerRegistrar registrar) {
        registrar.handler("/marytts/" + this.lang + this.country + "/" + this.voice,
                TextRequest.class, this::textToSpeech);
    }
}